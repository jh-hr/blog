---
title: Woensdag 30 Mei
date: 2018-05-30
categories: ["Design Challenge 4"]
tags: ["blogpost"]
---
#### Woensdag 30 Mei
Vandaag liet ik mijn high-fid prototype testen door Robin, Chantal, Hannad en Nikola. Hier had ik wat goede feedback van gekregen en heb ik gelijk de schermen aangepast. De prototype is klaar voor de expo. Ik heb ook een testplan en testrapport voor de tests van vandaag geschreven. 