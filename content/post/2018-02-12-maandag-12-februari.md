---
title: Maandag 12 Februari
date: 2018-02-12
categories: ["Design Challenge 3"]
tags: ["blogpost"]
---
#### Maandag 12 Februari
Vandaag hebben we nieuwe teams gekregen. Dit kwartaal zit ik gewoon in mijn eigen studio, best wel jammer sinds ik de sfeer in de andere studio fijner vind. Vandaag heb ik ook echt voor het eerst een “proper look” naar de opdracht gehad. Ik vind het niet de meest interessante onderwerp maar het geeft wel echt een eerstejaars onderwerp vibe af. Vandaag hebben we een teaminventarisatie gedaan en hebben we gelijk ook een PvA en een SCRUM bord gemaakt.