---
title: Dinsdag 5 September
date: 2017-09-05
categories: ["Design Challenge 1"]
tags: ["blogpost", "individueel onderzoek"]
---

#### Dinsdag 5 September 

Vandaag hebben we in de WdKA gewerkt want daar zit de mediatheek, eveneens ons doelgroep. Omdat er allerlei kunstopleidingen op het WdKA aanwezig zijn hebben we besloten om een definitieve doelgroep te kiezen: fotografiestudenten! Ook hebben we een vaag idee voor het concept, maar dat gaan we nog verder uitwerken.
![](individueel-onderzoek.png)