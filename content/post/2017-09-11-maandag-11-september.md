---
title: Maandag 11 September
date: 2017-09-11
categories: ["Design Challenge 1"]
tags: ["blogpost"]
---

#### Maandag 11 September
Vandaag hebben we onze concept verder uitgewerkt en even gekeken wat er allemaal op de schermpjes van onze paper prototype moet komen. Verder hadden we meer uitleg gekregen over de deliverables. Voor mijn gevoel ontbreekt de teamwork een beetje in de team, we werken wel samen maar tegelijkertijd zijn we heel erg veel individueel bezig. Ik ben een beetje bang dat hierdoor er minder samenhang is tussen de schermpjes (en functies/ideeën/regels) van onze prototype. Want mensen gaan nu maar een beetje verzinnen wat er op hun schermpjes komt, we voegen nog functies toe zonder dat we dit bespreken met elkaar en we keuren het maar goed. Ik vind deze manier van werken niet prettig maar de rest lijkt het allemaal wel goed te vinden. Misschien dat ik het later probeer op te brengen met de groep. 