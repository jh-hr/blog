---
title: Maandag 25 September
date: 2017-09-25
categories: ["Design Challenge 1"]
tags: ["blogpost"]
---

#### Maandag 25 September
Vandaag hebben we een ontwerpproceskaart gemaakt van onze proces tot nu toe. We begonnen nu pas een beetje laat aan de spelanalyses. We hadden Uno en Cards Against Humanity gespeeld, we hebben lekker veel gelachen. Vandaag gingen we verder aan onze concept. We hadden nog niet echt een nieuw concept, mensen bleven zich een beetje vastklampen aan het oude idee en kwamen met variaties hierop. Nadat ik zei dat we die hele idee in de prullenbak moesten gooien was de reactie een beetje totaal niet enthousiast. Nadat we met Elske hadden gepraat besloten we om een ander doelgroep te kiezen: illustratiestudenten aan de WdKA. Nog steeds met kunst dus! Daarna deden we nog een derde spelanalyse: Agar.io. Ook had ik nog wat gebrainstormed en nagedacht over nieuwe ideetjes.