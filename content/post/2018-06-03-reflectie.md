---
title: Reflectie
date: 2018-06-03
categories: ["Design Challenge 4"]
tags: ["blogpost", "Reflectie"]
---
#### Reflectie

DC4 zit erop! Ik ben blij dat ik er eindelijk klaar mee ben. Wat kan ik zeggen over deze kwartaal? Ik heb meer moeite gestoken in samenwerken en communiceren vergeleken met DC3, het was best wel vermoeiend. Ik heb heel veel geschetst en dingen gemaakt deze kwartaal, het was leuk en uitdagend maar ik hoop dat ik hier wat van leer en dingen echt wat eerder ga doen in het volgend schooljaar. Die laatste sprintje van het kwartaal is altijd zo zwaar. Het samenwerken verliep redelijk, maar iedereen was erg gefocust op competenties behalen. Een beetje jammer want we werkten daardoor heel erg zelfstandig. Mijn groepje was niet de meest ideale groepje, maar ik was dan ook niet de meest ideale teamgenoot. Toch hebben we uiteindelijk een mooi product op tafel kunnen zetten en daar ben ik best wel trots op. 

Hopelijk haal ik al mijn competenties en kan ik van een vroege vakantie genieten!
Bedankt CMD! Ik heb al een aantal nieuwe doelen voor het nieuwe schooljaar bedacht en ik kijk er nu al naar uit. 
