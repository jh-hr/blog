---
title: Reflectie
date: 2018-03-30
categories: ["Design Challenge 3"]
tags: ["blogpost", "Reflectie"]
---
#### Reflectie
Deze periode was heel raar voor mij. Ik had last van persoonlijke omstandigheden, dit heeft zeker niet geholpen maar ik ben hiervoor met mensen in gesprek geweest. Werken met mensen die ik niet ken vind ik heel ongemakkelijk, deze team voelde dus best wel… ongemakkelijk voor mij, maar dat is dus iets waar ik aan moet werken. Mijn communicatieve vaardigheden zijn best wel slecht voor mijn gevoel en dat bleek dus hieruit. Ik was heel erg stil, misschien deels ook omdat ik andere dingen aan mijn hoofd had maar goed. Ik heb veel dingen geleerd over het maken van infographics, dit kwam vooral doordat ik hier een workshop voor had gevolgd en ik heb een keuzevak over infographics. Ik heb dit kwartaal ook mijn eerste echte infographic gemaakt, daar ben ik best wel blij om. 
Mijn verbeterpunten voor dit kwartaal: beter plannen, meer om feedback vragen, actiever houding en beter communiceren.
