---
title: Maandag 9 Oktober
date: 2017-10-09
categories: ["Design Challenge 1"]
tags: ["blogpost"]
---

#### Maandag 9 Oktober

Iteratie 3 begon vandaag, we zijn bijna bij de finish-line! Voor deze iteratie moest het vooral "Keep It Simple, Stupid" zijn. Dus niet teveel onnodige dingen aan toevoegen. We moesten dus ook realistisch zijn met onze verwachtingen voor het uiteindelijk product. Helaas waren ook vandaag een aantal mensen niet aanwezig en kwamen niet veel verder. 

Verder heb ik vandaag nagedacht over hoe we de prototype gaan maken en heb ik wat dingen opgeschreven. Ik durf alleen niets concreets te verzinnen voordat er meer mensen zijn met wie ik kan praten over onze laatste prototype.