---
title: Maandag 5 Maart
date: 2018-03-05
categories: ["Design Challenge 3"]
tags: ["blogpost"]
---
#### Maandag 5 Maart
Vandaag is de deadline voor de lifestyle diary. Ik heb mijn lifestyle diary helemaal getekend en er een foto van gemaakt. Uiteindelijk was dat veels te veel moeite en had ik spijt dat ik het heb getekend. Mijn handen deden er echt pijn van omdat het zoveel was. In ieder geval, ik heb het ingeleverd.
