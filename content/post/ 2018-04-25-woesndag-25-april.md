---
title: Woensdag 25 April
date: 2018-04-25
categories: ["Design Challenge 4"]
tags: ["blogpost"]
---
#### Woensdag 25 April
Vandaag zijn we naar de markt in Afrikaanderwijk gegaan. Hier hebben we door de wijk gewandeld en foto's gemaakt van dingen die opvielen. We hebben ook rondgekeken op de markt, het was er druk maar onze doelgroep was nergens te bekennen. We hebben gekeken naar wat er verkocht werd en het zag er allemaal goed en goedkoop uit. Waarom zouden studenten hier niet hun boodschappen doen? Lag het misschien aan de tijd, komen ze later? We besloten om een ander keer weer terug te komen.