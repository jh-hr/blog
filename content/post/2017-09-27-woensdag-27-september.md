---
title: Woensdag 27 September
date: 2017-09-27
categories: ["Design Challenge 1"]
tags: ["blogpost"]
---


#### Woensdag 27 September
We hebben vandaag wat creatieve technieken gedaan, long list- short list. Hieruit kwam ik tenminste met niks waarvan ik dacht, ja dit kan wel. Vandaag liet ik in de groep mijn concept weer zien, dit keer opgeschreven op papier met wat tekeningen om de game beter te laten zien. Dit werd positief ontvangen maar voor mijn gevoel hebben we nog steeds geen definitief concept gekozen. Mensen zijn heel vaag en soms heb ik het gevoel dat we echt niet productief bezig zijn, mensen zijn meer bezig met hun individuele dingen. Ik ben maar stilletjes verder gaan denken over mijn concept nadat ik mijn nieuwe moodboard had gemaakt. 

![](MOODBOARD2-v2.png)
![](20171027_100203.jpg)