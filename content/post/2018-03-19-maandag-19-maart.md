---
title: Maandag 19 Maart
date: 2018-03-19
categories: ["Design Challenge 3"]
tags: ["blogpost"]
---
#### Maandag 19 Maart
Vandaag zijn we naar de validatiemoment geweest van de lifestyle diary, we hebben nuttige feedback gekregen. Alleen duurde het wel heel lang voordat we aan de beurt waren. Ook waren er geruchten dat deze leraar heel streng was, maar ik vond het meevallen. Hij gaf goede constructieve feedback en vetelde ons niet letterlijk wat we erin moesten zetten. Ik denk zodat we hierover gaan nadenken in plaats van klakkeloos overnemen.
