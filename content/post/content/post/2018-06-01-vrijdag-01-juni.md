---
title: Vrijdag 01 Juni
date: 2018-06-01
categories: ["Design Challenge 4"]
tags: ["blogpost"]
---
#### Vrijdag 01 Juni
Vandaag was de expo! De presentatie, conceptposter en high fid prototype werden vandaag gevalideerd. We kregen veel feedback van leraren op onze concept. We hadden helaas geen prijs gekregen voor onze concept maar dat is oke, ik verwachtte het ook niet echt. Ik heb wat toelichting gegeven over het concept aan mensen toen mijn teamgenoten even rondliepen, dat was best wel eng om te doen. Ik ben blij dat het er nu allemaal bijna op zit. Hopelijk haal ik alle competenties nog en dan lekker vakantie vieren.