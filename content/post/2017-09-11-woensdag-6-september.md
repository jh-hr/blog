---
title: Woensdag 6 September
date: 2017-09-06
categories: ["Design Challenge 1"]
tags: ["blogpost", "moodboard"]
---
#### Woensdag 6 September

Vandaag beginnetje aan het visualiseren van individueel onderzoek gemaakt, ik begon met wat schetsen maken voor de layout. Idee is nog een beetje vaag dus ik ga nog kijken hoe andere mensen het doen. Ik dacht er wel aan om het een beetje infographic-achtig te maken want ik vind dat er altijd leuk uitzien en overzichtelijk. Daarnaast heb ik een moodboard over fotografiestudenten gemaakt en hierover feedback gevraagd. Ik kreeg als feedback te horen dat het zoals het nu is goed is, alleen 1 element was iets minder sterk. Ook hebben we vandaag geleerd over hoe te bloggen, gitlab is een beetje lastig maar ik heb het werkend gekregen. Toen ik het eenmaal werkend had gekregen mocht ik de rest van mijn groepje hiermee helpen.

![](Moodboard-fotografiestudent.png)