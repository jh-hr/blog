---
title: Maandag 02 Oktober
date: 2017-10-02
categories: ["Design Challenge 1"]
tags: ["blogpost"]
---



#### Maandag 2 Oktober

Vandaag hebben we dus eindelijk een concept gekozen, we gingen gelijk aan de slag met onze paper prototype want dat moest echt gebeuren. Vandaag hadden we ook nog 3 relevante spelanalyses gedaan voor onze concept. Ik heb de spelanalyse voor Drawful geschreven. We moesten elkaar ook peerfeedback over samenwerking geven. Ik durfde niet zo heel goed minnetjes aan mensen te geven dus ik liet dingen maar blanco staan als ik geen mening had of een minnetje wou geven. Ik denk dat dit mede kwam doordat mensen niet echt minnetjes aan elkaar gaven, dus als ik als enige minnetjes ga geven voelt dat raar. Ik weet wel dat het goed is om constructieve feedback te geven aan mensen maar ik voel me niet 100% op mijn gemak in dit groepje hiervoor. Ik had overigens nog wat extra dingen aan onze concept toegevoegd zodat de studenten elkaar beter leren kennen.
