---
title: Maandag 9 April
date: 2018-04-09
categories: ["Design Challenge 4"]
tags: ["blogpost"]
---
#### Maandag 9 April
Vandaag was de kick-off voor DC4. Vandaag ontbraken er een aantal teamleden, maar desondanks moesten we wel een wijk kiezen voor het project. Uiteindelijk bleek dat we Carnisselande hadden gekregen. Ik had wat gegoogeld om een beeld te krijgen van de wijk. Verder heb ik gepraat over de competentietafels met klasgenoten.