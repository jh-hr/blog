---
title: Maandag 23 Oktober
date: 2017-10-23
categories: ["Design Challenge 1"]
tags: ["blogpost"]
---
#### Maandag 23 Oktober

De stress begint nu wel een beetje te komen. We gingen even hard aan de bak en zijn begonnen aan de dingen die we moeten opleveren. Er zijn schermen gemaakt, pdf'jes met dingen, presentaties, inhoudelijke dingen voor het concept. Ik heb de spelregels herschreven en heb hier en daar mensen geholpen. 