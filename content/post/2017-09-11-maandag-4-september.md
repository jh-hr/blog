---
title: Maandag 4 September
date: 2017-09-04
categories: ["Design Challenge"]
tags: ["blogpost", "individueel onderzoek"]
---

#### Maandag 4 September

Tijdens onze eerste echte werkgroep hebben we een teamcaptain aangesteld, teamregels opgesteld en nagedacht over wat we willen bereiken als een team. Ook hebben we een taakverdeling gemaakt en een teamposter gemaakt waarop we allemaal onze eigen kwaliteiten en ambities hebben geschreven. 

Voor het opdracht hadden we een thema gekozen: kunst in Rotterdam. Hiernaast besloten we dat onze doelgroep dan eerstejaars studenten aan het WdKA is.
Het thema en de doelgroep was nog te breed, dus vervolgens besloten we om individueel onderzoek te gaan doen naar kunst in Rotterdam. Ik deed onderzoek naar fotografie in Rotterdam.
We hadden ook onze studiecoach ontmoet en beginnetje gemaakt met algemene vragen voor een interview voor onze doelgroep.

![](20171004_142207.jpg)