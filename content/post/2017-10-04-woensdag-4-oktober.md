---
title: Woensdag 4 Oktober
date: 2017-10-04
categories: ["Design Challenge 1"]
tags: ["blogpost"]
---

#### Woensdag 4 Oktober

Vandaag was het presentatiedag + deadlinedag #2, we moesten voor andere klassen en alumni gaan presenteren. Helaas waren er vandaag weer mensen afwezig maar op het allerlaatste moment kwam iemand die de presentatie zou doen nog binnen. Dat was fijn want anders had ik of iemand anders heel last minute nog moeten presenteren en als ik het moest doen dan zou het rampzalig aflopen... waarschijnlijk. 

De presentatie ging prima, de feedback die we kregen was ook handig. Misschien een beetje verwarrend omdat het zo anders en uitgebreid was dan vorige feedback. Dat was natuurlijk wel fijn voor ons. Het was ook best wel leuk om de presentaties van andere teams te zien, er was zoveel variatie!

Na de presentaties was er een lunch waarbij je eigenlijk moest socializen... maar uiteindelijk bleef ik bij mijn groepje en hadden we maar aan onze blog gewerkt.
Overigens hadden we ookal netjes onze dingen de dag van te voren ingeleverd, lekker geen stress voor ons.