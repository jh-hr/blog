---
title: Maandag 12 Maart
date: 2018-03-12
categories: ["Design Challenge 3"]
tags: ["blogpost"]
---
#### Maandag 12 Maart
Vandaag hebben we om feedback gevraagd voor de ontwerpcriteria. Ik heb de feedback hiervoor genoteerd omdat Sander, die de ontwerpcriteria had gemaakt, er niet was. Zo kon hij ook nog feedback krijgen. Deze feedback heb ik netjes in een word bestand gezet en online gegooid. Vandaag hebben we een beginnetje gemaakt aan de lifestyle diary door wat papiertjes aan elkaar te plakken en te praten over onze bevindingen. Ook heb ik criteria geschreven voor onze lifestyle diary en deze online gezet. Vandaag had ik ook een workshop over Data-visualisatie van Mieke. Dit was best wel nutteloos maar ik heb wel wat infographics kunnen opzoeken voor mijn keuzevak dus het was niet een tijdverspilling.
