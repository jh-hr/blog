---
title: Woensdag 11 Oktober
date: 2017-10-11
categories: ["Design Challenge 1"]
tags: ["blogpost"]
---

#### Woesndag 11 Oktober

Niet iedereen was aanwezig vandaag. We hadden een gesprek met een van onze leraren over hoe we alles moesten aanpakken en besloten Lindsay en ik een checklist te maken, sinds we nog niet zo ver waren met het project. De checklist heb ik gelijk online gegooid. Op gegeven kwam onze teamleider alsnog op school en hadden we gepraat en beginnetjes gemaakt aan schermen en het prototype.

Vervolgens maakten we afspraken om in de herfstvakantie aan dingen te gaan werken, maar ik verwacht er niet zoveel van.