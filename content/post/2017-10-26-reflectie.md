---
title: Reflectie
date: 2017-10-26
categories: ["Design Challenge 1"]
tags: ["blogpost", "Reflectie"]
---
#### Reflectie

Ik vond dat deze project niet geweldig ging, er was veel miscommunicatie of helemaal geen communicatie in het groepje. Samenwerking was best wel desastreus. Mensen waren afwezig, meerdere malen (veels te veel eigenlijk) maar het waren allemaal lastige situaties waarbij je niet zo makkelijk een goed oordeel kan geven of iets oke was of niet. De team was wel aardig ondanks al dit. In ieder geval heb ik veel dingen geleerd over samenwerken en ben ik weer eens geconfronteerd met mijn verbeterpunten: communicatie en presenteren. Hoe leg ik het beste dingen uit? Hoe zeg ik dat iets beter moet? Hoe zorg ik ervoor dat mensen weten dat iets niet kan? Dat iets niet klopt? Dat dat ene kleine detail eigenlijk anders moet? Ik vind het nog best wel lastig en eng om met mensen te praten, dit is ook iets wat ik gewoon moet blijven oefenen. Hetzelfde geldt voor presenteren.

Desondanks alle problemen kijk ik uit naar P2. Ik wil graag nieuwe dingen leren en mijn vaardigheden ontwikkelen en hopelijk kom ik in een groepje terecht waar mensen niet zo vaak afwezig zijn.
![](Omnia.png)